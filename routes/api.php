<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->name('api.')->group(function(){
    Route::get('/candidatos', 'CandidatoController@index')->name('cadidatos');
    Route::post('/candidatos', 'CandidatoController@store')->name('candidatos_post');
    Route::get('/candidatos/{id}', 'CandidatoController@show');
    Route::put('/candidatos/{id}', 'CandidatoController@update');
    Route::delete('/candidatos/{id}', 'CandidatoController@delete');
});
