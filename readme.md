# Como rodar o projeto com docker

## Primeiro clone o projeto
```
git clone https://gitlab.com/lks.lucasvasconcelos/secif-banck-end.git

cd secif-back-end
```
## Configure o arquivo .env
```
- Crie um arquivo chamado ".env" e copie todo conteúdo do arquivo ".env.example" para ele
- Em seguida, altere os valores das variável de ambiente no arquivo ".env", de acordo com o que está definido no seu arquivo docker-compose

    DB_CONNECTION=mysql
    DB_HOST=db
    DB_PORT=3306
    DB_DATABASE=laravel
    DB_USERNAME=laravel
    DB_PASSWORD=secret

```

## Apagando imagens (Lembrar de se certificar que não tem outros containers rodando)

```
docker system prune -a
docker volume prune
```
## Suba os containers 

```
docker-compose up -d
```

## Entre no bash do container app
```
docker exec -it app bash
```

## Agora rode os seguintes comandos

```
composer install --no-scripts
composer dump
php artisan key:generate
php artisan migrate
```

```
- Pronto!! Sua aplicação estará disponível em localhost:8080
```