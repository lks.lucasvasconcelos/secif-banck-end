<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Candidato;



class CandidatoController extends Controller
{
    	// /**
	//  * @var Product
	//  */
	// private $product;
	// public function __construct(Product $product)
	// {
	// 	$this->product = $product;
	// }

    public function index(){
        return Candidato::all();
   }
   
   public function show(Candidato $id){
       return $id;
   }

   public function store(Request $request){
       $candidatoData = $request->all();
       Candidato::create($candidatoData);
   }

   public function update(Request $request, $id){
       $candidatoData = $request->all();
       $candidato = Candidato::find($id);
       $candidato->update($candidatoData);
   }

   public function delete(Candidato $id){
       $id->delete();
   }

}
